package com.cfreeland;

import org.apache.ibatis.session.SqlSession;

import com.cfreeland.resources.MyBatisTest;
import com.cfreeland.resources.TestObj;
import com.cfreeland.resources.TestObjMapper;

public class Application {

	public static void main(String[] args) {
		TestObj test = getTest();
		System.out.println(test.getName());
	}

	public static TestObj getTest() {
		SqlSession sqlSession = MyBatisTest.getSqlSessionFactory().openSession();
		try {
			TestObjMapper testMapper = sqlSession.getMapper(TestObjMapper.class);
			return testMapper.getById(2);
		} finally {
			sqlSession.close();
		}
	}
}
