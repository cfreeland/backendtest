package com.cfreeland.resources;

public interface TestObjMapper {

	public void insert(TestObj test);

	public TestObj getById(Integer id);
}
