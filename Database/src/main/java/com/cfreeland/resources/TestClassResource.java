package com.cfreeland.resources;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

@Path("/print")
public class TestClassResource {

	@GET
	public Response print(){
		return Response.ok("{\"header\":\"Hello World\",\n\"message\":\"Lorem ipsum dolor sit amet\"}")
				.header("Access-Control-Allow-Origin", "*")
				.build();
	}
	
	@GET
	@Path("moar")
	public Response printMoar() {
		return Response.ok("{\"message\":\"Here's some more\"}")
				.header("Access-Control-Allow-Origin", "*")
				.build();
	}
}
