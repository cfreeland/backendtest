package com.cfreeland.resources;

public class TestObj {
	private int id;
	private String name;
	
	public TestObj(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	
	public TestObj() {}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
}
